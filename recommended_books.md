* [Livros recomendados](#livros-recomendados)


# Livros recomendados
- Understanding marxism (Geoff Boucher)
- How to hide an empire (Daniel Immerwahr)
- Hitler ganhou a guerra (Walter Graziano)
- Sociedade do espetáculo (Guy Debord)
- Sociedade do cansaço (Byung Chul-Han)
- Casta: as origens de nosso mal estar (Isabel Wilkerson)
- Macroeconomics (Bill Mitchell)
- Realismo capitalista (Mark Fisher)
- As veias abertas da América Latina (Eduardo Galeano)
- Racismo estrutural (Silvio Almeida)
- Sintomas mórbidos: A encruzilhada da esquerda brasileira (Sabrina Fernandes)
- Revolução Africana: Uma antologia do pensamento marxista (Jones Manoel)
- The Socialist System: The Political Economy of Communism (Janos Kornai)